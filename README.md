# Introduction

The [`@electrum-cash/application`](https://www.npmjs.com/package/@electrum-cash/application) library provides a robust framework for building SPV-validated tools and services.

## Features

- Full SPV-validation of monitored transactions and blocks.
- Rich notifications for each step in the transaction, block or address life-cycle.
- You can ask for partial data without needing to understand how to acquire it.
- Automatic handling of network edge-cases, fail-over and opportunistic caching.

For complete information on all typings, arguments and options, read to the [documentation](https://electrum-cash.gitlab.io/application/) generated from the source code.

## Trade-offs

- Full SPV-validation raises network, memory and processing costs.
- Opportunistic caching raises storage and memory costs.
- Automatic handling of network edge-cases reduces privacy control.

# Usage

## Installation

Installation is easy, just get the library from NPM:

```sh
npm install @electrum-cash/application
```

## Setup

```ts
// Import library features.
import { initializeElectrumApplication } from '@electrum-cash/application';

// Initialize the electrum application.
const electrumEvents = await initializeElectrumApplication('My Electrum Application');
```

## Tracking the blockchain

Once you have initialized the electrum application you can monitor when new blocks become available.

```ts
// Log when new block headers are available.
electrumEvents.on('BlockReceived', console.log);

// Log when new block headers have been verified.
electrumEvents.on('BlockVerified', console.log);

// Log what chain height and percent of the chain has been verified.
electrumEvents.on('ChainStatus', console.log);
```

### Blockchain Life-Cycle

...

## Tracking transactions

After you monitor an address or transaction, you get events for each step in each transactions life-cycle.

```ts
// Log when new transactions are available.
electrumEvents.on('TransactionReceived', console.log);

// Log when transactions are verified.
electrumEvents.on('TransactionVerified', console.log);

// Log when transactions are doublespent.
electrumEvents.on('TransactionContested', console.log);

// Log when transactions are finalized.
electrumEvents.on('TransactionFinalized', console.log);

// Log when transactions are rejected.
electrumEvents.on('TransactionRejected', console.log);

// Either monitor a specific transaction...
await monitorTransaction(transactionHash);

// .. or all pending transactions for a specific address.
await monitorAddress(address);
```

### Transaction Life-Cycle

From the perspective of the `@electrum-cash/application` library, transactions are handled as a finite state system with a linear progression from detection to either finalization or rejection.
However, rejected transactions can re-enter the mempool under certain conditions and can therefor go through the process more than one time.

```mermaid
graph LR
  unknown@{ shape: circ }
  finalized@{ shape: dbl-circ }
  discarded@{ shape: dbl-circ }
style finalized stroke: #0f3
style discarded stroke: #f30
  unknown ---> |validated|received
  received --> |not contested|verified;
  verified --> finalized;
  verified --> |excluded|discarded;
  received --> |doublespent|contested;
  contested --> |included|verified;
  contested --> |dropped|discarded;
  discarded ---> |rebroadcasted|unknown;
```

#### Normal operation

Transactions are initially `unknown`.

When a transaction is **validated** and indexed by the electrum server, it changes state to `received`, and a {@link TransactionReceived} event is emitted.

At this point the transaction is part of the mempool and scheduled for inclusion in a future block.

After some time, assuming the transaction is **not contested**, a miner creates a block that includes the transaction and the state change to `verified`, and a {@link TransactionVerified} event is emitted.

If the block the transaction was included in is orphaned on the network, the transaction reverts back to its previous `received` state, and a {@link TransactionReceived} event is emitted.

When a sufficient number of additional blocks have been mined on top of the block that included the transaction, the state changes to `finalized` and a {@link TransactionFinalized} event is emitted.

*Note that while a chain re-organization can technically revert any transaction, this library makes the assumption that a finalized transaction can never be reverted.*

#### Double-spends

If a conflicting transaction is detected while the transaction is in the mempool, the state changes to `contested`, and a {@link TransactionContested} event is emitted.

From this point, if the original transaction is **included** in a block, the state changes to `verified`, a {@link TransactionVerified} event is emitted, and the transaction resumes normal operation.

If, however, the competing transaction is mined in a block, the original transaction is **dropped** from the mempool and a {@link TransactionRejected} event is emitted.

If the block the transaction was included in is orphaned on the network, the transaction reverts back to its previous `contested` state, and a {@link TransactionContested} event is emitted.

*Note that in rare circumstances, a **dropped** transaction may be **rebroadcasted** later as a new transaction, if the chain experiences a re-organization event that makes the transaction valid again.*

#### Chain re-organizations

If a `verified` transaction is part of a block that is **orphaned**, the state is reverted to it's previous state and an event is emitted matched the reverted state, for example:

- a transaction that is verified, but has never been contested, reverts to the `received` state and an {@link TransactionReceived} event is emitted.
- a transaction that is verified, but previously was contested, reverts to the `contested` state and an {@link TransactionContested} event is emitted.

If a conflicting transaction is mined into a block while a transaction is in the `verified` state, it is `excluded` from the blockchain and a {@link TransactionRejected} event is emitted.

*Note that in rare circumstances, an **excluded** transaction may be **rebroadcasted** later as a new transaction, if the chain experiences a re-organization event that makes the transaction valid again.*


## Tracking address history

After you monitor an address, you get events when an address history is updated.

```ts
// Log when the address history has changed.
electrumEvents.on('AddressUpdateReceived', console.log);

// TODO: Add events for address balance updates.

// Monitor a specific address.
await monitorAddress(address);
```

### Address Life-Cycle

...

## Requesting Data

## Getting balance, UTXOs and transaction history.

...

```ts
```

## Support

If you find bugs or have problems using this library, you can file issues on the [Gitlab](https://gitlab.com/electrum-cash/application/issues).
