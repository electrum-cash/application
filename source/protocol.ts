import { hexToBin } from '@bitauth/libauth';

/**
 * Set up a constant to determine how many further blocks are required as confirmations before we consider the current block and all related transactions immutable.
 *
 * This is set to match the "10 block rolling checkpoint" or "deep re-org protection" rule introduced by Bitcoin ABC and inherited to BCHN.
 *
 * Assuming a chain of 12 blocks, starting at X and increasing to X+11, the following should hold true about the finalization rule:
 * - Until X+10, all transaction in X are mutable.
 * - At X+10, all transaction in X are immutable.
 */
export const BLOCKS_REQUIRED_FOR_FINALIZATION = 10;

// TODO: Document me.
export const MAX_TARGET_EXPONENT_DATA = 0x1d;
export const MIN_TARGET_SIGNIFICAND = 0x008000;
export const MAX_TARGET_SIGNIFICAND = 0x7fffff;

// Export the maximum compressed difficulty target.
export const MAX_COMPRESSED_DIFFICULTY_TARGET = hexToBin('1d00ffff');

// Export ASERT difficulty adjustment activation time and height.
export const ASERT_ANCHOR_HEIGHT = 1;
export const ASERT_ACTIVATION_TIME_MTP = 1;

// Export CW144 difficulty adjustment activation height.
export const CW144_ACTIVATION_HEIGHT = 1;
