// Import network access from electrum-cash.
import { ElectrumWebSocket } from '@electrum-cash/web-socket';
import { electrumServers as defaultServers } from '@electrum-cash/servers';
import { electrumCheckpoint as defaultCheckpoint } from '@electrum-cash/checkpoint';

// Import electrum network abstractions
import { electrumClients, initializeElectrumNetwork, stopElectrumNetwork } from './network';

// import transaction state management.
import
{
	getTransactionState,
	setTransactionAsReceived,
	setTransactionAsRejected,
	setTransactionAsContested,
	setTransactionAsVerified,
	setTransactionAsOrphaned,
	setTransactionAsFinalized,
	verifyTransactionInclusion,
} from './transactions';

import
{
	fetchBlockHeaders,
	fetchHistory,
	subscribeToBlockheaderUpdates,
	subscribeToAddressUpdates,
	subscribeToTransactionUpdates,
	subscribeToDoublespendUpdates,
} from '@electrum-cash/protocol';

// ..
import { electrumEvents } from './events';
import { configureDatabase, storeChainStateInCache, storeBlockHeadersInCache, getChainStateFromCache, getAddressHistoryFromCache, storeAddressHistoryInCache } from './cache';
import { getBlockHashFromHeader } from './headers';
import { getTransaction, getBlockHeaderFromHeight } from './data';
import { BLOCKS_REQUIRED_FOR_FINALIZATION } from './protocol';

// Import required type definitions.
import type { ElectrumServers } from '@electrum-cash/servers';

// Import electrum and blockchain related type information.
import type { BlockHeight, BlockHeaderHex, BlockHeaderHash, HeadersSubscribeNotification, Address, AddressStatus, AddressGetHistoryResponse, AddressSubscribeNotification, TransactionHash, TransactionSubscribeNotification, TransactionDsProofNotification } from '@electrum-cash/protocol';

// Import local interfaces.
import { ElectrumOptions, ElectrumApplicationEvents, ChainStatus, BlockState, BlockStages } from './interfaces';

// Import thread locking library to control order of operations.
import { Mutex, MutexInterface } from 'async-mutex';

//
import type { EventEmitter } from 'eventemitter3';

// Export local interfaces, as it is required to generate full documentation.
export * from './interfaces';

/**
 * @module ElectrumApplication
 */

// Initialize an empty in-memory transaction and block state tracker.
const internalBlockState: Record<BlockHeaderHash, BlockState> = {};

// Utility function to initialize internal block state, if necessary.
async function initializeInternalBlockState(blockHeaderHash: BlockHeaderHash, blockHeight: BlockHeight): Promise<void>
{
	// Initialize the internal block state, if necessary.
	if(typeof internalBlockState[blockHeaderHash] === 'undefined')
	{
		internalBlockState[blockHeaderHash] =
		{
			height: blockHeight,
			confirmations: 0,
			contested: false,
			finalized: false,
			stage: BlockStages.RECEIVED,
		};
	}
}

// Initialize a mutex lock for the synchronization of block headers.
const headerSynchronizationLock = new Mutex();

// Initialize a default empty chain status.
let currentChainStatus: ChainStatus = { currentHeight: 0, verifiedHeight: 0, verifiedPercent: '--.-' };

/**
 * Internal utility function to handle unrecoverable system errors in a predictable manner:
 *
 * - Reports on the problem
 * - Stops active timers and intervals.
 * - Stops background tasks.
 * - Disconnects from all networks.
 *
 * Note that it does not stop the currently running process, to allow upstream software to gracefully shut down.
 *
 * @internal
 */
function systemFailure(reason: string): void
{
	// Set up an error message for legibility.
	const errorMessage = `Shutting down @electrum-cash/application due to an unrecoverable system error: ${reason}`;

	// Emit system failure message.
	electrumEvents.emit('SystemFailure', errorMessage);

	// Log that we are shutting down the application layer.
	console.error(errorMessage);

	// Cancel all timers.
	// NOTE: We currently have no setTimeout or setIntervals in here.

	// Request the header synchronization lock in order to prevent further work to validate the blockchain.
	// Note that we do not await this intentionally, so that we can shut down the network below, which will
	// result in a synchronization failure and release of the current lock.
	headerSynchronizationLock.acquire();

	// Disconnect from all networks.
	// NOTE: This allows the electrum service to drop all ongoing subscriptions.
	stopElectrumNetwork();
}

/**
 * Subscribes to block updates and emits events when the chain status updates.
 * @note This is called automatically by {@link initializeElectrumApplication}.
 * @group Blockchain
 */
export async function monitorBlockchain(): Promise<void>
{
	// For each server we are connected to..
	for(const electrumClient of electrumClients.values())
	{
		// Set up a subscription to monitor for blockchain header updates.
		await subscribeToBlockheaderUpdates(electrumClient);
	}
}

/**
 * Subscribes to address updates and emits events when the address history changes.
 *
 * Note that any transaction that relates to the address history, will automatically be monitored as well.
 * @group Address
*
 * @param address - The address to monitor for changes.
 */
export async function monitorAddress(address: Address): Promise<void>
{
	// For each server we are connected to..
	for(const electrumClient of electrumClients.values())
	{
		// Set up a subscription to monitor for the address for status updates.
		await subscribeToAddressUpdates(electrumClient, address);
	}
}

/**
 * Subscribes to transaction updates and emits events when the transaction status changes.
 *
 * ```mermaid
 * graph LR
 *   unknown ---> |validated|received
 *   received --> |not contested|verified;
 *   verified --> finalized;
 *   verified --> |excluded|discarded;
 *   received .-> |doublespent|contested;
 *   contested --> |included|verified;
 *   contested --> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 *   unknown ---> |received|pending
 *   pending ---> |doublespent|contested;
 *   pending -----> |not contested|verified -....-> |orphaned|pending;
 *   contested --> |dropped|discarded;
 *   contested ---> |included|verified -..-> |orphaned|contested;
 *   verified --> |excluded|discarded;
 *   verified --> finalized;
 * ```
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style unknown stroke: #f30
 *   unknown ---> |received|pending;
 *   pending --> |doublespent|contested;
 *   pending -..-> |dropped|unknown;
 *   contested -.-> |rejected|unknown;
 *   pending --> |included|verified;
 *   contested --> |included|verified;
 *   verified-.-> |orphaned|unknown;
 *   verified --> |finalized|finalized;
 * ```
 * @group Transaction
 *
 * @param transactionHash - The transaction to monitor for changes.
 */
export async function monitorTransaction(transactionHash: TransactionHash): Promise<void>
{
	// For each server we are connected to..
	for(const electrumClient of electrumClients.values())
	{
		// Set up a subscription to monitor for block inclusion.
		await subscribeToTransactionUpdates(electrumClient, transactionHash);

		// Also monitor for double-spend proofs for the transaction.
		await subscribeToDoublespendUpdates(electrumClient, transactionHash);
	}
}

/**
 * Utility function to get the current chain status without needing to respond to a {@link ChainStatus} event.
 * @group Blockchain
 */
export async function getChainStatus(): Promise<ChainStatus>
{
	return currentChainStatus;
}

/**
 * Utility function to calculate a percentage for chain recovery status.
 * @internal
 */
async function calculateChainRecoveryPercent(internalBlockHeight: BlockHeight, externalBlockHeight: BlockHeight): Promise<number>
{
	return ((internalBlockHeight - defaultCheckpoint.height) / (externalBlockHeight - defaultCheckpoint.height)) * 100;
}

/**
 * Calculates a chain recovery percent, then if different from currently stored value emits and event and updates the stored value.
 * @internal
 */
async function updateCurrentChainStatus(blockHeight: BlockHeight, targetBlockHeight: BlockHeight): Promise<void>
{
	// Calculate the current chain recovery percent after accepting the block as valid.
	const chainRecoveryPercent = await calculateChainRecoveryPercent(blockHeight, targetBlockHeight);
	const chainRecoveryPercentTruncated = Math.floor(10 * chainRecoveryPercent) / 10;
	const chainRecoveryPercentFixed = chainRecoveryPercentTruncated.toFixed(1);

	// Determine if the recovery percentage has changed significantly.
	const recoveryPercentHasChangeSignificantly = (chainRecoveryPercentFixed !== currentChainStatus.verifiedPercent);

	// Determine if the new height if for a fully recovered state.
	const recoveryPercentIsAt100Percent = (chainRecoveryPercentFixed === '100.0');
	const updatedHeightIsDifferent = (blockHeight !== currentChainStatus.verifiedHeight);
	const updatedHeightAt100Percent = (recoveryPercentIsAt100Percent && updatedHeightIsDifferent);

	// If the recovery state has changed or we are updating at full recovery..
	if(recoveryPercentHasChangeSignificantly || updatedHeightAt100Percent)
	{
		// Update the current chain status.
		currentChainStatus =
		{
			currentHeight: targetBlockHeight,
			verifiedHeight: blockHeight,
			verifiedPercent: chainRecoveryPercentFixed,
		};

		// Issue an event indicating that chain status has been updated.
		electrumEvents.emit('ChainStatus', currentChainStatus);
	}
}

/**
 * Internal function to abstract and optimize performance when storing block headers.
 * @internal
 */
async function storeBlockHeader(blockHeaderHash: BlockHeaderHash, blockHeaderHex: BlockHeaderHex, blockHeight: BlockHeight): Promise<void>
{
	// Shorten the block header hash.
	const shortenedBlockheaderHash = blockHeaderHash.replace(/^0+/, '');

	// Arrange the block header hex and hash for storage.
	const blockHeaderWithHexAndHash =
	{
		blockHeaderHex: blockHeaderHex,
		blockHeaderHash: shortenedBlockheaderHash,
		blockHeaderHeight: blockHeight,
	};

	// Return a promise for when the block has been fully stored.
	return storeBlockHeadersInCache([ blockHeaderWithHexAndHash ]);
}

/**
 * Handles incoming block headers by verifying their validity, storing them in a cache and emitting related events.
 * @internal
 */
async function processBlockHeader(blockHeight: BlockHeight, blockHeader: BlockHeaderHex, targetBlockHeight: BlockHeight): Promise<void>
{
	// Calculate the full and shortened hash of the block header.
	const blockheaderHash = await getBlockHashFromHeader(blockHeader);

	// Store the blocks header, height and hash in the local cache.
	await storeBlockHeader(blockheaderHash, blockHeader, blockHeight);

	// Set up initial block state for this header, if necessary.
	await initializeInternalBlockState(blockheaderHash, blockHeight);

	// Send a signal indicating that we have received a new block.
	electrumEvents.emit('BlockReceived', { blockHash: blockheaderHash, blockState: internalBlockState[blockheaderHash] });

	// TODO: Validate chainwork? (processBlockHeader is only called from subscription responses, which should always be above the checkpoint height)

	// TODO: Send a signal indicating that we have validated a new block.
	// TODO: Verify that this is all information we want to send here.
	// electrumEvents.emit('BlockVerified', { blockHeight, blockHash: blockheaderHash });
}

// Initialize an empty in-memory transaction mutex lock tracker.
const transactionUpdateLocks: Record<TransactionHash, Mutex> = {};

/**
 * Utility function to allow transaction updates to run without race conditions.
 */
async function acquireTransactionStateLock(transactionHash: TransactionHash): Promise<MutexInterface.Releaser>
{
	// Create a transaction update lock mutex for this transaction if none currently exist.
	if(!transactionUpdateLocks[transactionHash])
	{
		transactionUpdateLocks[transactionHash] = new Mutex();
	}

	// Acquire a mutex lock for updates on this transaction and return the release function.
	return transactionUpdateLocks[transactionHash].acquire();
}

/**
 * Handles incoming transaction updates by validating their block inclusion status, storing in the cache and emitting related events.
 */
async function processTransactionUpdate(transactionHash: TransactionHash, transactionInclusionHeight: BlockHeight): Promise<void>
{
	// Acquire a mutex lock preventing concurrent transaction updates for the same transaction.
	const unlockTransactionUpdates = await acquireTransactionStateLock(transactionHash);

	try
	{
		// Determine the current transaction chain inclusion state.
		const transactionIsDropped  = (transactionInclusionHeight === null);
		const transactionInMempool  = (transactionInclusionHeight <= 0);
		const transactionIsIncluded = (transactionInclusionHeight > 0);

		// Get the current transaction state.
		const transactionState = await getTransactionState(transactionHash);

		// Determine the current transaction stages for legibility.
		const transactionIsUnknown   = !transactionState.received;
		const transactionIsReceived  = transactionState.received;
		const transactionIsVerified  = transactionState.verified;
		const transactionIsFinalized = transactionState.finalized;

		// Trigger system failure event if block finalization assumption is broken.
		if(transactionIsFinalized && !transactionIsIncluded)
		{
			await systemFailure(`Transaction ${transactionHash} was orphaned after being finalized by ${BLOCKS_REQUIRED_FOR_FINALIZATION} confirmations.`);

			// Consider this transaction update fully processed.
			return;
		}

		// If the transaction was previously known (this includes verified state), but is no longer available..
		if(transactionIsReceived && transactionIsDropped)
		{
			// Remove any lingering transaction state.
			await setTransactionAsRejected(transactionHash);

			// Consider this transaction update fully processed.
			return;
		}

		// If the transaction was previously verified to be in a block, but have since been orphaned..
		if(transactionIsVerified && transactionInMempool)
		{
			// Mark the transaction as orphaned.
			await setTransactionAsOrphaned(transactionHash);

			// TODO: emit blockchain re-organization state message. (issue #23)

			// For each server we are connected to..
			for(const electrumClient of electrumClients.values())
			{
				// Resubscribe to double-spend updates, as those expired on previous block inclusion.
				// NOTE: We do not need to wait for this to return before moving on.
				subscribeToDoublespendUpdates(electrumClient, transactionHash);
			}

			// Consider this transaction update fully processed.
			return;
		}

		// Handle this transaction, if it has not been received before.
		if(transactionIsUnknown)
		{
			// Set the internal tracking of this transactions state to received.
			// NOTE: This also marks all outputs it spends as spent.
			await setTransactionAsReceived(transactionHash);

			// Get the transaction to ensure it gets cached.
			await getTransaction(transactionHash);
		}

		// NOTE: The CONTESTED (doublespend) state is between RECEIVED and VERIFIED, but is handled by processDoublespendNotification().

		// Skip block inclusion validation for this transaction if it is either unknown, or in the mempool.
		// NOTE: Electrum provides 'null' for unknown, and a height of '0' or less for in-mempool.
		if((transactionInclusionHeight === null) || (transactionInclusionHeight <= 0))
		{
			return;
		}

		// Verify that the transaction is included in the block.
		await verifyTransactionInclusion(transactionHash, transactionInclusionHeight);

		// Handle transaction verification if this is the first time the transaction has confirmations.
		if(!transactionIsVerified)
		{
			// Hash the block header to get the block identity.
			const blockHeaderHex = await getBlockHeaderFromHeight(transactionInclusionHeight);
			const blockHeaderHash = await getBlockHashFromHeader(blockHeaderHex);

			// Mark the transaction as verified.
			await setTransactionAsVerified(transactionHash, blockHeaderHash);
		}

		// Handle transaction finalization if this is the first time the transaction has sufficient confirmations.
		if(!transactionIsFinalized)
		{
			// Determine how many blocks have been mined that confirms this transaction.
			const transactionVerificationDepth = currentChainStatus.currentHeight - transactionInclusionHeight;

			// Finalize the transaction is it has sufficient block confirmations.
			if(transactionVerificationDepth >= BLOCKS_REQUIRED_FOR_FINALIZATION)
			{
				await setTransactionAsFinalized(transactionHash);
			}
		}
	}
	catch(error)
	{
		// Log errors with transaction updates, for now.
		console.error(`Failed to process transaction update: ${error}`);
	}
	finally
	{
		// Release the mutex locks for this transaction.
		unlockTransactionUpdates();
	}
}

/**
 * Handles incoming address updates by tracking the address history, unspent outputs and balances, storing transactions in a cache and emitting related events.
 * @internal
 */
async function processAddressUpdate(address: Address, status: AddressStatus): Promise<void>
{
	electrumEvents.emit('AddressUpdate', { address, status });

	// Determine what block height we already have sufficient address history for.
	const { addressHistory: cachedAddressHistory, blockHeight } = await getAddressHistoryFromCache(address);

	// Fetch history since last update (or all if this is the first request)
	// TODO: Handle clustering (issue #24)
	// NOTE: Clustering is hard to implement here because servers that are behind may still provide a valid history, rather than an error.
	const [ firstElectrumClient ] = electrumClients.values();
	const updatedAddressHistory = await fetchHistory(firstElectrumClient, address, blockHeight);

	// Get the internal block height from cache.
	const internalBlockHeight = await getChainStateFromCache('currentHeight');

	// Determine the block height from which we will consider address history immutable.
	const immutableBelowHeight = (internalBlockHeight - BLOCKS_REQUIRED_FOR_FINALIZATION);

	// Set up a list of address history entries to finalize.
	const addressHistoryToFinalize: Map<TransactionHash, BlockHeight> = new Map();

	// Set up a list of empty transaction update promises.
	const transactionUpdatePromises = [];

	// Iterate over history to:
	for(const { tx_hash: transactionHash, height } of updatedAddressHistory)
	{
		// Monitor the transaction if it is still in the mempool.
		if(height <= 0)
		{
			// Request updates for this transaction in the future.
			// NOTE: In rare cases, this might monitor an already monitored transactions and that is not a problem.
			// NOTE: We do not await this in order to ensure fast processing of address histories.
			monitorTransaction(transactionHash);
		}

		// If the transaction should be considered an immutable entry in the address history..
		if((height >= 1) && (height < immutableBelowHeight))
		{
			// Add the transaction to address history to finalize.
			addressHistoryToFinalize.set(transactionHash, height);
		}

		// Process the transaction as if it a transaction update event was emitted.
		// NOTE: We do not await this in order to ensure fast processing of address histories.
		transactionUpdatePromises.push(processTransactionUpdate(transactionHash, height));
	}

	// If there is any transactions to finalize in the the history cache...
	if(addressHistoryToFinalize.size > 0)
	{
		// Initialize an empty set of finalized address history.
		const finalizedAddressHistory: AddressGetHistoryResponse = [];

		// add the history to finalize to the finalized history object.
		for(const [ TransactionHash, height ] of addressHistoryToFinalize.entries())
		{
			finalizedAddressHistory.push({ tx_hash: TransactionHash, height });
		}

		// Determine the highest block height in the finalized history.
		const highestBlockHeight = Math.max(...addressHistoryToFinalize.values());

		// Store the finalized address history.
		await storeAddressHistoryInCache(address, { ...cachedAddressHistory, ...finalizedAddressHistory }, highestBlockHeight);
	}

	// Wait for all transaction updates to complete.
	await Promise.all(transactionUpdatePromises);

	// TODO: Use the updated address data to: (issue #27)
	// - determine a current UTXO set for the address
	// - determine a current balance based on the UTXO set.
	//   · emit event at end of process if balance changed
}

/**
 * Requests block headers in a loop until the internal and external block height is synchronized.
 * @internal
 */
async function synchronizeBlockHeaders(targetBlockHeight: BlockHeight, targetBlockheader: BlockHeaderHex): Promise<void>
{
	// Throw an error if the reported chain height is less than the checkpoint height.
	if(targetBlockHeight < defaultCheckpoint.height)
	{
		throw(new Error(`Network blockchain height (${targetBlockHeight}) is lower than the known-good checkpoint height (${defaultCheckpoint.height}).`));
	}

	// If synchronization is currently ongoing, do nothing.
	if(await headerSynchronizationLock.isLocked())
	{
		// Issue an event indicating that chain status has been updated.
		electrumEvents.emit('ChainStatus', currentChainStatus);

		// Return and let current synchronization continue on it's own.
		return;
	}

	// Lock the header synchronization lock to prevent concurrent synchronization of headers.
	const unlock = await headerSynchronizationLock.acquire();

	try
	{
		// Get the internal block height from cache.
		let internalBlockHeight = await getChainStateFromCache('currentHeight');

		// Emit an event to indicate the current chain status, if it has changed.
		await updateCurrentChainStatus(internalBlockHeight, targetBlockHeight);

		// While there are block header to synchronize between our internal state and the provided block..
		while(internalBlockHeight < targetBlockHeight)
		{
			// Determine what the next block height will be.
			const startingBlockHeight = internalBlockHeight + 1;

			// Count the number of blocks there are to download.
			const remainingBlocks = targetBlockHeight - internalBlockHeight;

			// Set up a list of request promises in order to get failover and lowest latency when doing header requests.
			const requestPromises = [];

			// Request headers from all connected clients.
			for(const electrumClient of electrumClients.values())
			{
				requestPromises.push(fetchBlockHeaders(electrumClient, startingBlockHeight, Math.min(50, remainingBlocks)));
			}

			// Use the first successful response available.
			const blockHeaders = await Promise.any(requestPromises);

			// Set up a list of promises so we can process block headers in parallel.
			const processedBlockHeaderPromises = [];

			// Process each of the new block headers
			for(const blockHeader of blockHeaders)
			{
				// Determine what the next block height will be.
				const nextBlockHeight = internalBlockHeight + 1;

				// Process the provided block header as the next in the chain.
				const processBlockHeaderPromise = processBlockHeader(nextBlockHeight, blockHeader, targetBlockHeight);

				// Add the promise to the list.
				processedBlockHeaderPromises.push(processBlockHeaderPromise);

				// Bump the internal block height to the next block.
				internalBlockHeight = nextBlockHeight;
			}

			// Wait for all blocks to have been successfully processed.
			await Promise.all(processedBlockHeaderPromises);

			// Store the updated internal block height.
			await storeChainStateInCache('currentHeight', internalBlockHeight);

			// Emit an event to indicate the current chain status, if it has changed.
			await updateCurrentChainStatus(internalBlockHeight, targetBlockHeight);
		}
	}
	catch(error)
	{
		// Do nothing.
		console.warn('Failed to synchronize block headers, will retry later:', error);
	}
	finally
	{
		// Unlock the thread so further synchronization can happen.
		unlock();
	}
}

/**
 * Utility function that handles incoming address notification messages.
 * @internal
 */
async function processAddressNotification(electrumServer: string, notification: AddressSubscribeNotification): Promise<void>
{
	// Extract the address and status from the notification.
	const { params } = notification;
	const [ address, status ] = params;

	// Do nothing if the address have no status.
	// TODO: Verify if this works.
	if(status === null)
	{
		return;
	}

	// TODO: Do nothing if the status has not changed since the last event.
	// if(status ==== currentAddressStatus[address])
	// {
	// 	return;
	// }

	// Process the address update.
	await processAddressUpdate(address, status);
}

/**
 * Utility function that handles incoming header notification messages.
 * @internal
 */
async function processHeaderNotification(electrumServer: string, notification: HeadersSubscribeNotification): Promise<void>
{
	// Extract the block header and height from the notification.
	const { params } = notification;
	const { height, hex } = params[0];

	// TODO: Check for re-orgs when height moves backwards.

	// Synchronize up to this block in the chain.
	synchronizeBlockHeaders(height, hex);
}

/**
 * Utility function that handles incoming transaction notification messages.
 * @internal
 */
async function processTransactionNotification(electrumServer: string, notification: TransactionSubscribeNotification): Promise<void>
{
	// Extract the transaction hash and inclusion height from the notification.
	const { params } = notification as TransactionSubscribeNotification;
	const [ transactionHash, inclusionHeight ] = params;

	// Process the transaction update.
	await processTransactionUpdate(transactionHash, inclusionHeight);
}

/**
 * Utility function that handles incoming double-spend notification messages.
 * @internal
 */
async function processDoublespendNotification(electrumServer: string, notification: TransactionDsProofNotification): Promise<void>
{
	// Extract the double spend proof from the notification.
	const { params } = notification;
	const doubleSpendProof = params.slice(-1)[0];

	// Do nothing if no double-spend proof is available.
	if(doubleSpendProof === null)
	{
		return;
	}

	// Mark the transaction as contested.
	await setTransactionAsContested(doubleSpendProof.txid, doubleSpendProof.dspid);

	// Send a signal indicating that the provided transaction hash has been doublespent.
	// NOTE: Double spend proofs are taken into account for the transaction and address status updates, so no further action is necessary.
	electrumEvents.emit('TransactionContested', { transactionHash: doubleSpendProof.txid, transactionState: await getTransactionState(doubleSpendProof.txid) });
}

/**
 * Initializes a light client that implements the Simple Payment Verification protocol transparently for the user.
 * @group Setup
 *
 * @param application - name of the application to identify as with the servers.
 * @param options     - optional configuration settings.
 *
 * @returns an event emitter for the electrum application events.
 *
 * @emits
 *
 * - {@link AddressBalance} TODO: Document me.
 * - {@link AddressHistory} TODO: Document me.
 * - {@link BlockReceived} when a new block has been detected.
 * - {@link BlockVerified} when a new block has been validated.
 * - {@link ChainStatus} when new blocks have been verified to be a part of the longest chain.
 * - {@link TransactionReceived} when a new transaction has been detected.
 * - {@link TransactionContested} when a transaction has received a doublespend proof.
 * - {@link TransactionVerified} when a transaction has been verified to be a part of the current chain.
 * - {@link TransactionFinalized} when a verified transaction has sufficient confirmations in the current chain.
 * - {@link TransactionRejected} when a transaction is removed from the mempool and/or blockchain.
 *
 * @example const electrumEvents = await initializeElectrumApplication('YourAppNameHere');
 */
export async function initializeElectrumApplication(application: string, options?: ElectrumOptions): Promise<EventEmitter<ElectrumApplicationEvents>>
{
	// Configure storage layer before first use.
	await configureDatabase({ ...options });

	// ...
	const defaultElectrumOptions =
	{
		electrumServers: defaultServers,
		electrumEncryption: true,
	};

	// Merge the provided options with the defaults.
	const currentOptions = { ...defaultElectrumOptions, ...options };

	// Extract the final transport scheme and electrum servers.
	const { electrumServers, electrumEncryption } = currentOptions;

	// Create a connection with the electrum network.
	await initializeElectrumNetwork(application, electrumServers, electrumEncryption);

	// For each server we are connected to..
	for(const [ electrumServer, electrumClient ] of electrumClients.entries())
	{
		// Listen for notifications
		electrumClient.on('blockchain.headers.subscribe', processHeaderNotification.bind(this, electrumServer));
		electrumClient.on('blockchain.address.subscribe', processAddressNotification.bind(this, electrumServer));
		electrumClient.on('blockchain.transaction.subscribe', processTransactionNotification.bind(this, electrumServer));
		electrumClient.on('blockchain.transaction.dsproof.subscribe', processDoublespendNotification.bind(this, electrumServer));

		// Listen for network events.
		electrumClient.on('connecting', electrumEvents.emit.bind(electrumEvents, 'connecting'));
		electrumClient.on('connected', electrumEvents.emit.bind(electrumEvents, 'connected'));
		electrumClient.on('reconnecting', electrumEvents.emit.bind(electrumEvents, 'reconnecting'));
		electrumClient.on('disconnecting', electrumEvents.emit.bind(electrumEvents, 'disconnecting'));
		electrumClient.on('disconnected', electrumEvents.emit.bind(electrumEvents, 'disconnected'));
	}

	// Resume validation from the current chain state.
	const internalBlockHeight = await getChainStateFromCache('currentHeight');

	// Reset to the checkpoint height if no chain state exist.
	if(!internalBlockHeight)
	{
		await storeChainStateInCache('currentHeight', defaultCheckpoint.height);
	}

	// Initialize the current chain status to our internal height, keeping the verified percent to 0 for now.
	currentChainStatus.verifiedHeight = internalBlockHeight || defaultCheckpoint.height;

	// Issue an event with our initial chain status.
	electrumEvents.emit('ChainStatus', currentChainStatus);

	// TODO: Get and validate internal state (checkpoint, blockheight, chainwork etc)

	// Set up tracking for blockheader events.
	await monitorBlockchain();

	// Return the event emitter.
	return electrumEvents;
}
