/**
 * @group Address
 */
/*
export async function getAddressHistory(address: Address): Promise<any>
{
	// Determine what block height we already have sufficient address history for.
	const { addressHistory, blockHeight } = await getAddressHistoryFromCache(address);

	// Fetch history since last update (or all if this is the first request)
	// TODO: Handle clustering (issue #24)
	// NOTE: Clustering is hard to implement here because servers that are behind may still provide a valid history, rather than an error.
	const [ firstElectrumClient ] = electrumClients.values();
	const pendingAddressHistory = await fetchHistory(firstElectrumClient, address, blockHeight);

	// Merge the finalized and pending address history together
	const currentAddressHistory = [ ...addressHistory, ...pendingAddressHistory ];

	// Return the current address history.
	return currentAddressHistory;
}
*/

/**
 * Provides a list of all fungible tokens, including satoshis, on unspent outputs for the given address.
 * @group Address
 */
/*
export async function getAddressBalance(address: Address): Promise<any>
{
	// TODO: Find unspent outputs by lockscript / address.
	// TODO: Iterate over each unspent output and tally the satoshis and fungible token amounts.
	// TODO: Return all fungible balances.
}
*/

/**
 * Provides a list of all non-fungible tokens on unspent outputs for the given address.
 * @group Address
 */
/*
export async function getAddressTokens(address: Address): Promise<any>
{
	// TODO: Find unspent outputs by lockscript / address.
	// TODO: Iterate over each unspent output and list all non-fungible tokens with their commitments.
	// TODO: Return the list of non-fungible tokens.
}
*/
