// Import dependencies and typing.
import { EventEmitter } from 'eventemitter3';
import type { ElectrumApplicationEvents } from './interfaces';

/**
 * Set up an event emitter for SPV related events.
 * @internal
 */
export const electrumEvents: EventEmitter<ElectrumApplicationEvents> = new EventEmitter();
