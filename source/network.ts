// Import network access.
import { initializeElectrumClient, sufficientProtocolVersion, requiredProtocolVersion } from '@electrum-cash/protocol';

// Import default electrum socket.
import { ElectrumWebSocket } from '@electrum-cash/web-socket';

// Import required type definitions.
import type { ElectrumClient } from '@electrum-cash/network';
import type { ElectrumServers } from '@electrum-cash/servers';
import type { ElectrumProtocolEvents } from '@electrum-cash/protocol';

/**
 * Export the electrum network providers.
 * @internal
 */
export const electrumClients: Map<string, ElectrumClient<ElectrumProtocolEvents>> = new Map();

/**
 * @internal
 */
export async function initializeElectrumNetwork(application: string, servers: ElectrumServers, encrypted: boolean): Promise<void>
{
	// Add servers to the cluster.
	for(const server of servers)
	{
		// Ignore any servers that do not support the required version.
		if(!await sufficientProtocolVersion(server.version))
		{
			// eslint-disable-next-line no-console
			console.warn(`Skipping ${server.host} due to insufficient version (${server.version} < ${requiredProtocolVersion}).`);
			continue;
		}

		// Determine the transport name.
		const transport = (encrypted ? 'wss' : 'ws');

		// ...
		const port = server.transports[transport];

		// Ignore any servers that do not support encrypted websockets.
		if(!server.transports[transport])
		{
			// eslint-disable-next-line no-console
			console.warn(`Skipping ${server.host} due to missing '${transport}' transport.`);
			continue;
		}

		// Skip this servers if it is already part of the network.
		if(electrumClients.has(server.host))
		{
			console.warn(`Skipping ${server.host} as it is already a part of the initialized network.`);
			continue;
		}

		// Create and initialize an appropriate socket type.
		const socket = new ElectrumWebSocket(server.host, port, encrypted);

		// Create an electrum client that connects using the provided socket.
		const electrumClient = await initializeElectrumClient(application, socket);

		// Add the electrum client to the list of initialized network clients.
		electrumClients.set(server.host, electrumClient);
	}
}

/**
 * Disconnects from the electrum network and cleans up related resources.
 * @internal
 */
export async function stopElectrumNetwork(): Promise<void>
{
	// Disconnect all servers.
	for(const electrumClient of electrumClients.values())
	{
		electrumClient.disconnect(true);
	}

	// Remove all servers.
	electrumClients.clear();
}
