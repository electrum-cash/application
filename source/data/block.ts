// Import cache related functions.
import { getBlockHeaderFromCache, getBlockHeaderAtHeightFromCache, getHeightForBlockHeaderFromCache, storeBlockHeadersInCache } from '../cache';

// Import network related functions.
import { fetchBlockHeaderWithProofFromBlockHeight } from '@electrum-cash/protocol';

// Import utility functions.
import { verifyBlockInclusionProof } from '../chain';
import { getBlockHashFromHeader } from '../headers';

// Import the electrum client list.
import { electrumClients } from '../network';

// Import types
import type { BlockHeight, BlockHeaderHash, BlockHeaderHex } from '@electrum-cash/protocol';

// Import the checkpoint necessary to validate block inclusion.
import { electrumCheckpoint } from '@electrum-cash/checkpoint';

/**
 * @module ElectrumApplication
 */

/**
 * Gets a raw block header as a hex-encoded string, based on the provided block header hash.
 * @group Blockchain
 *
 * @param blockHeaderHash {BlockHeaderHash} - hash of the block header to use as an identifier.
 *
 * @returns {Promise<BlockHeaderHex>} the block header as a hex-encoded string.
 */
export async function getBlockHeader(blockHeaderHash: BlockHeaderHash): Promise<BlockHeaderHex>
{
	// Shorten the block header hash.
	const shortenedBlockheaderHash = blockHeaderHash.replace(/^0+/, '');

	// Lookup the block header in local cache.
	const cachedBlockHeaderHex = await getBlockHeaderFromCache(shortenedBlockheaderHash);

	// Return the block header from cache.
	if(cachedBlockHeaderHex)
	{
		return cachedBlockHeaderHex;
	}

	// Throw an error to indicate that the requested data is currently unavailable.
	throw(new Error(`Could not provide raw block header for ${blockHeaderHash}: Either the block does not exist in the indexer's chain, or the header has not yet been processed locally.`));
}

/**
 * Gets the block height where the provided block header hash was included in the chain.
 * @group Blockchain
 *
 * @param blockHeaderHash     {BlockHeaderHash}   hash of the block header to store.
 *
 * @returns {Promise<BlockHeaderHex>}
 */
export async function getBlockHeightFromHash(blockHeaderHash: BlockHeaderHash): Promise<BlockHeight>
{
	// Shorten the block header hash.
	const shortenedBlockheaderHash = blockHeaderHash.replace(/^0+/, '');

	// Lookup the block height in local cache.
	const cachedBlockHeight = await getHeightForBlockHeaderFromCache(shortenedBlockheaderHash);

	// Return the block height from cache.
	if(cachedBlockHeight)
	{
		return cachedBlockHeight;
	}

	// Throw an error to indicate that the requested data is currently unavailable.
	throw(new Error(`Could not provide raw block header for ${blockHeaderHash}: Either the block does not exist in the indexer's chain, or the header has not yet been processed locally.`));
}

/**
 * Gets a raw block header as a hex-encoded string, based on the provided block height.
 * @group Blockchain
 *
 * @param blockHeight   {BlockHeight}   block height of the block
 *
 * @returns {Promise<BlockHeaderHex>}
 */
export async function getBlockHeaderFromHeight(blockHeight: BlockHeight): Promise<BlockHeaderHex>
{
	// Lookup the block header in local cache.
	const cachedBlockHeaderHash = await getBlockHeaderAtHeightFromCache(blockHeight);

	// Return the block header from cache.
	if(cachedBlockHeaderHash)
	{
		// NOTE: The cached header will be shortened with leading zeroes removed.
		//       These are not re-added here since this function would then need to remove them.
		return getBlockHeader(cachedBlockHeaderHash);
	}

	// Throw an error if we are unable to validate this header.
	if(blockHeight > electrumCheckpoint.height)
	{
		throw(new Error(`Could not validate a block header on height ${blockHeight}: The requested header is ahead of the current checkpoint at height ${electrumCheckpoint.height}`));
	}

	// Prepare a list of request promises.
	const requestPromises = [];

	// Send a network request to each connected server.
	for(const electrumClient of electrumClients.values())
	{
		requestPromises.push(fetchBlockHeaderWithProofFromBlockHeight(electrumClient, blockHeight, electrumCheckpoint.height));
	}

	// Extract the full block header and merkle proof for this height from the first successful response.
	const { branch, header, root } = await Promise.any(requestPromises);

	// Hash the fetched block header
	const fetchedBlockHeaderHash = await getBlockHashFromHeader(header);
	const shortenedBlockheaderHash = fetchedBlockHeaderHash.replace(/^0+/, '');

	try
	{
		// Validate chain inclusion
		await verifyBlockInclusionProof(fetchedBlockHeaderHash, blockHeight, branch, root);
	}
	catch(error)
	{
		// Re-throw the error with more context.
		throw(new Error(`Failed to validate block header ${fetchedBlockHeaderHash} at height ${blockHeight}: validation of block inclusion in checkpoint at height ${electrumCheckpoint.height} failed.`));
	}

	// Arrange the block header hex and hash for storage.
	const blockHeaderWithHexAndHash =
	{
		blockHeaderHex: header,
		blockHeaderHash: shortenedBlockheaderHash,
		blockHeaderHeight: blockHeight,
	};

	// Store the block header.
	await storeBlockHeadersInCache([ blockHeaderWithHexAndHash ]);

	// Return the fetched block header.
	return header;
}
