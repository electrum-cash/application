// Import cache related functions.
import { getTransactionFromCache, getTransactionProofFromCache, storeTransactionInCache, storeTransactionProofInCache } from '../cache';

// Import network related functions.
import { fetchTransaction, fetchTransactionProof } from '@electrum-cash/protocol';

// Import the electrum client list.
import { electrumClients } from '../network';

// Import types
import type { BlockHeight, TransactionHash, TransactionHex, TransactionGetMerkleResponse } from '@electrum-cash/protocol';

/**
 * @module ElectrumApplication
 */

/**
 * Gets a raw transaction as a hex-encoded string.
 * @group Transaction
 *
 * @param transactionHash {TransactionHash} - hash of the transaction to use as an identifier.
 *
 * @returns {Promise<TransactionHex>} the transaction as a hex-encoded string.
 */
export async function getTransaction(transactionHash: TransactionHash): Promise<TransactionHex>
{
	// Lookup the transaction in local cache.
	const cachedTransactionHex = await getTransactionFromCache(transactionHash);

	// Return the transaction from cache.
	if(cachedTransactionHex)
	{
		return cachedTransactionHex;
	}

	// Prepare an empty list of transaction fetching promises.
	const requestPromises = [];

	// Fetch the transaction from each client.
	for(const electrumClient of electrumClients.values())
	{
		requestPromises.push(fetchTransaction(electrumClient, transactionHash));
	}

	// Wait for the first successful response.
	// NOTE: If all responses reject, this will result in a thrown error.
	const fetchedTransactionHex = await Promise.any(requestPromises);

	// Store the fetched transaction in local cache.
	await storeTransactionInCache(transactionHash, fetchedTransactionHex);

	// Return the transaction fetched from the network.
	return fetchedTransactionHex;
}

/**
 * Gets a transaction inclusion proof.
 * @group Transaction
 *
 * @param transactionHash {TransactionHash} - hash of the transaction to use as an identifier.
 * @param blockHeight {BlockHeight} - the height in the blockchain the transaction was included in.
 *
 * @returns {Promise<TransactionGetMerkleResponse | undefined>} the transaction inclusion proof, or undefined if it is still in the mempool.
 */
export async function getTransactionInclusionProof(transactionHash: TransactionHash, blockHeight: BlockHeight): Promise<TransactionGetMerkleResponse>
{
	// Determine a unique identity for this transaction proof to make sure it is immutable.
	const transactionProofIdentity = `${transactionHash}:${blockHeight}`;

	// Lookup the transaction proof in local cache.
	const cachedTransactionProof = await getTransactionProofFromCache(transactionProofIdentity);

	// Return the transaction proof from cache.
	if(cachedTransactionProof)
	{
		return cachedTransactionProof;
	}

	// Prepare an empty list of transaction proof fetching promises.
	const requestPromises = [];

	// Fetch the transaction proof from each client.
	for(const electrumClient of electrumClients.values())
	{
		requestPromises.push(fetchTransactionProof(electrumClient, transactionHash, blockHeight));
	}

	// Wait for the first successful response.
	// NOTE: If all responses reject, this will result in a thrown error.
	const fetchedTransactionProof = await Promise.any(requestPromises);

	// Return undefined if no proof it available.
	if(!fetchedTransactionProof)
	{
		return undefined;
	}

	// Store the fetched transaction proof in local cache.
	await storeTransactionProofInCache(transactionProofIdentity, fetchedTransactionProof);

	// Return the transaction proof fetched from the network.
	return fetchedTransactionProof;
}
