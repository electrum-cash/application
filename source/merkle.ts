// Import utility functions.
import { binToHex, sha256 } from '@bitauth/libauth';

/**
 * @module Utilities.Merkle
 * @internal
 */

/**
 * Verifies that a provided data hash at a given position is a part of a merkle tree.
 *
 * @param dataHash       {Uint8Array}          ...
 * @param dataPosition   {Uint8Array}          ...
 * @param merkleBranches {Array<Uint8Array>}   ...
 * @param merkleRootHash {Uint8Array}          ...
 *
 * @throws an error if the computed root hash does not match the provided merkle root hash.
 *
 * @returns {Promise<void>}
 */
export async function verifyMerkleProof(dataHash: Uint8Array, dataPosition: number, merkleBranches: Array<Uint8Array>, merkleRootHash: Uint8Array): Promise<void>
{
	// Copy the target block hash as the starting point in the merkle branch.
	let rootHashCandidate = dataHash;

	// Use the data position in the block as the merkle path, determining the orientation of the hashes at each step.
	let merklePath = dataPosition;

	// For each branch in the merkle proof..
	for(const branch of merkleBranches)
	{
		// Assume the default merkle orientation, where our data is on the left side of the branch.
		let merkleOrientation = [ ...rootHashCandidate, ...branch ];

		// Update merkle orientation if the merkle path indicates that we should be on the right side of the branch.
		if(merklePath % 2)
		{
			merkleOrientation = [ ...branch, ...rootHashCandidate ];
		}

		// Update the root hash candidate by hashing the merkle nodes at this branch in the tree.
		rootHashCandidate = sha256.hash(sha256.hash(merkleOrientation as unknown as Uint8Array));

		// Bit-shift out the right-most position in the merkle path as we've used it.
		merklePath >>= 1;
	}

	// Throw an error if the computed root hash does not match the expected merkle root.
	if(binToHex(rootHashCandidate) !== binToHex(merkleRootHash))
	{
		throw(new Error(`Provided dataHash (${binToHex(dataHash)}) either does not belong at the specified position (${dataPosition}) or does not belong in the the specified data set (${binToHex(merkleRootHash)}).`));
	}
}
