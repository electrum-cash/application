// Import utility functions to manage blockchain related structures.
import { hexToBin, binToHex, assertSuccess, decodeTransaction } from '@bitauth/libauth';

// Import application-wide event emitter.c
import { electrumEvents } from './events';

// Import utility functions to validate transaction inclusion proofs.
import { getMerkleRootFromHeader } from './headers';
import { getTransaction, getTransactionInclusionProof, getBlockHeaderFromHeight } from './data';
import { verifyTransactionInclusionProof } from './chain';

// Import local interfaces.
import type { TransactionState, OutputState } from './interfaces';

// Import electrum and blockchain related type information.
import type { InputIdentifier, InputIndex, OutputIdentifier, OutputIndex, TransactionHash, BlockHeight, BlockHeaderHash, DoubleSpendProofHash } from '@electrum-cash/protocol';

// Initialize an empty in-memory transaction and output state trackers.
const transactionState: Record<TransactionHash, TransactionState> = {};
const outputState: Record<OutputIdentifier, OutputState> = {};

/**
 * Ensures that internal transaction state exist for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to track state for.
 */
async function initializeTransactionState(transactionHash: TransactionHash): Promise<void>
{
	// Do nothing if we already have state for this transaction..
	if(typeof transactionState[transactionHash] !== 'undefined')
	{
		return;
	}

	// ..otherwise, set an initial transaction state.
	transactionState[transactionHash] =
	{
		received: false,
		contested: undefined,
		verified: undefined,
		finalized: false,
	};
}

/**
 * Ensures that internal output state exist for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
async function initializeOutputState(outputIdentifier: OutputIdentifier): Promise<void>
{
	// NOTE: We intentionally avoid initializing the dependent transaction state here, as it will be generated on-demand when needed if not already initialized.

	// Do nothing if we already have state for this output..
	if(typeof outputState[outputIdentifier] !== 'undefined')
	{
		return;
	}

	// ..otherwise, set an initial output state.
	outputState[outputIdentifier] =
	{
		spent: undefined,
	};
}

/* Utility functions to work with input and output identifiers. */

/**
 * Utility function to create an input identifier from a transaction and input index.
 * @group Input
 *
 * @param transactionHash - hash of the transaction that created the output.
 * @param inputIndex      - index of the input in the transaction.
 */
export async function getInputIdentifier(transactionHash: TransactionHash, inputIndex: InputIndex): Promise<InputIdentifier>
{
	return `${transactionHash}:${inputIndex}`;
}

/**
 * Utility function to parse an inputIdentifier into a transaction hash and index value.
 * @group Output
 *
 * @param inputIdentifier - the input identifier to parse.
 *
 * @returns an tuple with the transaction hash and input index.
 */
export async function parseInputIdentifier(inputIdentifier: InputIdentifier): Promise<[ TransactionHash, InputIndex ]>
{
	// Parse the identifier to get the transaction hash and index value.
	const [ transactionHash, inputIndex ] = inputIdentifier.split(':');

	// Return the parsed data.
	return [ transactionHash, Number(inputIndex) ];
}

/**
 * Utility function to create an output identifier from a transaction and output index.
 * @group Output
 *
 * @param transactionHash - hash of the transaction that created the output.
 * @param outputIndex     - index of the output in the transaction.
 */
export async function getOutputIdentifier(transactionHash: TransactionHash, outputIndex: OutputIndex): Promise<OutputIdentifier>
{
	return `${transactionHash}:${outputIndex}`;
}

/**
 * Utility function to parse an outputIdentifier into a transaction hash and index value.
 * @group Output
 *
 * @param outputIdentifier - the output identifier to parse.
 *
 * @returns an tuple with the transaction hash and output index.
 */
export async function parseOutputIdentifier(outputIdentifier: OutputIdentifier): Promise<[ TransactionHash, OutputIndex ]>
{
	// Parse the identifier to get the transaction hash and index value.
	const [ transactionHash, outputIndex ] = outputIdentifier.split(':');

	// Return the parsed data.
	return [ transactionHash, Number(outputIndex) ];
}

/**
 * Provides the internal transaction state for a given transaction.
 * @group Transaction
 *
 * @param transactionHash - hash of the transaction to get internal state of.
 *
 * @returns the internal state for the provided transaction.
 */
export async function getTransactionState(transactionHash: TransactionHash): Promise<TransactionState>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// Return the current transaction state.
	return transactionState[transactionHash];
}

/**
 * Provides the internal output state for a given output.
 * @group Output
 *
 * @param outputIdentifier - unique identifier for the output.
 *
 * @returns the internal state for the provided output.
 */
export async function getOutputState(outputIdentifier: OutputIdentifier): Promise<OutputState>
{
	// Ensure output state is fully initialized.
	await initializeOutputState(outputIdentifier);

	// Extract the transaction hash from the output identifier.
	const [ transactionHash ] = await parseOutputIdentifier(outputIdentifier);

	// Construct the full output state.
	const fullOutputState =
	{
		...transactionState[transactionHash],
		...outputState[outputIdentifier],
	};

	// Return the current output state.
	return fullOutputState;
}

/* Utility functions to parse transaction data. */

/**
 * Utility function to list all inputs of a given transaction.
 * @note this will also fetch and cache the transaction, if necessary.
 *
 * @param transactionHash - hash of the transaction to list inputs from.
 *
 * @returns a list of input identifiers.
 */
export async function listTransactionInputs(transactionHash: TransactionHash): Promise<Array<InputIdentifier>>
{
	// Initialize an empty list of input identifiers.
	const inputIdentifiers: Array<InputIdentifier> = [];

	// Fetch the transaction and extract the inputs.
	const transactionHex = await getTransaction(transactionHash);
	const transactionBin = await hexToBin(transactionHex);
	const { inputs } = await assertSuccess(await decodeTransaction(transactionBin));

	// For each input in the transaction..
	for(const { outpointTransactionHash, outpointIndex } of inputs)
	{
		// Convert the provided binary hash to string, then get the input identifier.
		const inputTransactionHash = await binToHex(outpointTransactionHash);
		const inputIdentifier = await getInputIdentifier(inputTransactionHash, outpointIndex);

		// Add the input identifier to the list.
		inputIdentifiers.push(inputIdentifier);
	}

	// Return the list of input identifiers.
	return inputIdentifiers;
}

/**
 * Utility function to list all outputs of a given transaction.
 * @note this will also fetch and cache the transaction, if necessary.
 *
 * @param transactionHash - hash of the transaction to list outputs from.
 *
 * @returns a list of output identifiers.
 */
export async function listTransactionOutputs(transactionHash: TransactionHash): Promise<Array<OutputIdentifier>>
{
	// Initialize an empty list of output identifiers.
	const outputIdentifiers: Array<OutputIdentifier> = [];

	// Fetch the transaction and extract the outputs.
	const transactionHex = await getTransaction(transactionHash);
	const transactionBin = await hexToBin(transactionHex);
	const { outputs } = await assertSuccess(await decodeTransaction(transactionBin));

	// For each output in the transaction..
	for(const outputIndex in outputs)
	{
		// Create an output identifier from the transaction hash and output index.
		const outputIdentifier = await getInputIdentifier(transactionHash, Number(outputIndex));

		// Add the output identifier to the list.
		outputIdentifiers.push(outputIdentifier);
	}

	// Return the list of output identifiers.
	return outputIdentifiers;
}

/* Utility functions to emit transaction related events. */

/**
 * Emits the transaction rejected event for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to emit the event for.
 */
export async function sendTransactionRejectedEvent(transactionHash: TransactionHash): Promise<void>
{
	electrumEvents.emit('TransactionRejected', { transactionHash });
}

/**
 * Emits the transaction received event for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to emit the event for.
 */
export async function sendTransactionReceivedEvent(transactionHash: TransactionHash): Promise<void>
{
	electrumEvents.emit('TransactionReceived', { transactionHash, transactionState: await getTransactionState(transactionHash) });
}

/**
 * Emits the transaction contested event for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to emit the event for.
 */
export async function sendTransactionContestedEvent(transactionHash: TransactionHash): Promise<void>
{
	electrumEvents.emit('TransactionContested', { transactionHash, transactionState: await getTransactionState(transactionHash) });
}

/**
 * Emits the transaction block inclusion event for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to emit the event for.
 */
export async function sendTransactionVerifiedEvent(transactionHash: TransactionHash): Promise<void>
{
	electrumEvents.emit('TransactionVerified', { transactionHash, transactionState: await getTransactionState(transactionHash) });
}

/**
 * Emits the transaction orphan event for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to emit the event for.
 */
export async function sendTransactionOrphanedEvent(transactionHash: TransactionHash): Promise<void>
{
	electrumEvents.emit('TransactionOrphaned', { transactionHash, transactionState: await getTransactionState(transactionHash) });
}

/**
 * Emits the transaction finalization event for a given transaction.
 * @internal
 *
 * @param transactionHash - hash of the transaction to emit the event for.
 */
export async function sendTransactionFinalizedEvent(transactionHash: TransactionHash): Promise<void>
{
	electrumEvents.emit('TransactionFinalized', { transactionHash, transactionState: await getTransactionState(transactionHash) });
}

/* Utility functions to emit output related events. */

/**
 * Emits the output received event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputReceivedEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputReceived', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/**
 * Emits the output spent event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputSpentEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputSpent', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/**
 * Emits the output rejected event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputRejectedEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputRejected', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/**
 * Emits the output contested event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputContestedEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputContested', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/**
 * Emits the output block inclusion event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputVerifiedEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputVerified', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/**
 * Emits the output orphan event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputOrphanedEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputOrphaned', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/**
 * Emits the output finalization event for a given output.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 */
export async function sendOutputFinalizedEvent(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Emit the event with the associated state.
	electrumEvents.emit('OutputFinalized', { outputIdentifier, outputState: await getOutputState(outputIdentifier) });
}

/* Utility functions to update the output stages. */

/**
 * Removes existing output state for mutable transactions.
 * @internal
 *
 * @param outputIdentifier - unique identifier for the output.
 *
 * @throws an error if the transaction is finalized, and therefor considered immutable.
 */
export async function setOutputAsUnknown(outputIdentifier: OutputIdentifier): Promise<void>
{
	// Ensure output state is fully initialized.
	await initializeOutputState(outputIdentifier);

	// Extract the transaction hash from the output identifier.
	const [ transactionHash ] = await parseOutputIdentifier(outputIdentifier);

	// Throw an error if this would break the finalization assumptions.
	if(transactionState[transactionHash].finalized)
	{
		throw(new Error(`Failed to set output (${outputIdentifier}) as unknown: cannot reset transaction output state after a transaction has finalized`));
	}

	// Delete existing transaction state.
	// TODO: Don't delete, just mark as unknown.
	delete outputState[outputIdentifier];
}

/**
 * Marks an output as spent.
 * @internal
 *
 * @param outputIdentifier - the output to be marked as spent.
 * @param inputIdentifier - the input that is spending the output.
 */
export async function setOutputAsSpent(outputIdentifier: OutputIdentifier, inputIdentifier: InputIdentifier): Promise<void>
{
	// Ensure output state is fully initialized.
	await initializeOutputState(outputIdentifier);

	// Do nothing if we already know that this is spent by the provided input.
	if(outputState[outputIdentifier].spent === inputIdentifier)
	{
		return;
	}

	// Update the spent flag to the spending input.
	outputState[outputIdentifier].spent = inputIdentifier;

	// Emit an output spent event.
	sendOutputSpentEvent(outputIdentifier);
}

/**
 * Marks an output as unspent.
 *
 * This is intended to be used when sending a transaction fail due to external reasons, such as when a transaction is dropped from the mempool.
 * @internal
 *
 * @param outputIdentifier - the output to be marked as unspent.
 * @param inputIdentifier - the input that is no longer spending the output.
 */
export async function setOutputAsUnspent(outputIdentifier: OutputIdentifier, inputIdentifier: InputIdentifier): Promise<void>
{
	// Ensure output state is fully initialized.
	await initializeOutputState(outputIdentifier);

	// Do nothing if we already know that this is output is not spent.
	// NOTE: we consider both undefined and false as unspent cases.
	if(!outputState[outputIdentifier].spent)
	{
		return;
	}

	// Do nothing if the output is spent by a different input than the one provided.
	if(outputState[outputIdentifier].spent !== inputIdentifier)
	{
		return;
	}

	// Update the spent flag to false to indicate that we explicitly say this output is not spent.
	outputState[outputIdentifier].spent = false;

	// Emit an output spent event.
	sendOutputReceivedEvent(outputIdentifier);
}

/* Utility functions to update the transaction stages. */

/**
 * Removes existing transaction state for mutable transactions.
 * @internal
 *
 * @param transactionHash - hash of the transaction to remove state for.
 *
 * @throws an error if the transaction is finalized, and therefor considered immutable.
 */
export async function setTransactionAsUnknown(transactionHash: TransactionHash): Promise<void>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// Throw an error if this would break the finalization assumptions.
	if(transactionState[transactionHash].finalized)
	{
		throw(new Error(`Failed to set transaction (${transactionHash}) as unknown: cannot reset transaction state after a transaction has finalized`));
	}

	// Delete existing transaction state.
	// TODO: Reconsider this, we might want to be able to hold on to information that a transaction existed, but reset the state or track why it was reset.
	delete transactionState[transactionHash];
}

/**
 * Marks the transaction as rejected.
 * @internal
 *
 * @param transactionHash - hash of the transaction that was rejected.
 */
export async function setTransactionAsRejected(transactionHash: TransactionHash): Promise<void>
{
	// Get a list of inputs and outputs.
	const transactionInputIdentifiers = await listTransactionInputs(transactionHash);
	const transactionOutputIdentifiers = await listTransactionOutputs(transactionHash);

	// Iterate over each input and mark as unspent by this transaction.
	for(const inputIdentifier of transactionInputIdentifiers)
	{
		await setOutputAsUnspent(inputIdentifier, transactionHash);
	}

	// Iterate over each output and send the output rejected event.
	for(const outputIdentifier of transactionOutputIdentifiers)
	{
		await sendOutputRejectedEvent(outputIdentifier);
	}

	// Remove current transaction state.
	await setTransactionAsUnknown(transactionHash);

	// Emit event indicating the transaction has been rejected.
	await sendTransactionRejectedEvent(transactionHash);
}

/**
 * Marks a transaction as received, if it is currently unknown.
 * @internal
 *
 * @param transactionHash - hash of the transaction to mark as received.
 */
export async function setTransactionAsReceived(transactionHash: TransactionHash): Promise<void>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// NOTE: This function does not throw any error even if the transaction is finalized, as no changes would be made in that case.

	// If the transaction is currently unknown..
	if(!transactionState[transactionHash].received)
	{
		// Update the transaction as received.
		transactionState[transactionHash].received = true;

		// Emit event indicating this was received for the first time.
		await sendTransactionReceivedEvent(transactionHash);

		// Get a list of inputs and outputs.
		const transactionInputIdentifiers = await listTransactionInputs(transactionHash);
		const transactionOutputIdentifiers = await listTransactionOutputs(transactionHash);

		// Iterate over each input and mark as spent by this transaction.
		for(const inputIdentifier of transactionInputIdentifiers)
		{
			await setOutputAsSpent(inputIdentifier, transactionHash);
		}

		// Iterate over each output and send the output received event.
		for(const outputIdentifier of transactionOutputIdentifiers)
		{
			await sendOutputReceivedEvent(outputIdentifier);
		}
	}
}

/**
 * Marks a currently verified and still mutable transaction as orphaned, indicating that it returned to the mempool.
 * @internal
 *
 * @param transactionHash - hash of the transaction to mark as orphaned.
 *
 * @throws an error if the transaction is finalized, and therefor considered immutable.
 * @throws an error if the transaction is not verified, and therefor cannot be orphaned.
 */
export async function setTransactionAsOrphaned(transactionHash: TransactionHash): Promise<void>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// Throw an error if this would break the finalization assumptions.
	if(transactionState[transactionHash].finalized)
	{
		throw(new Error(`Failed to set transaction (${transactionHash}) as orphaned: cannot update transaction with new state after a transaction has finalized`));
	}

	if(!transactionState[transactionHash].verified)
	{
		throw(new Error(`Failed to set transaction (${transactionHash}) as orphaned: cannot orphan transactions that have not been verified.`));
	}

	// Clear any previously set block inclusion proofs.
	transactionState[transactionHash].verified = undefined;

	// Emit an transaction orphaning event.
	await sendTransactionOrphanedEvent(transactionHash);

	// Get a list of inputs and outputs.
	const transactionInputIdentifiers = await listTransactionInputs(transactionHash);
	const transactionOutputIdentifiers = await listTransactionOutputs(transactionHash);

	// Iterate over each input and mark as no longer spent by this transaction.
	for(const inputIdentifier of transactionInputIdentifiers)
	{
		await setOutputAsUnspent(inputIdentifier, transactionHash);
	}

	// Iterate over each output and send the output orphaned event.
	for(const outputIdentifier of transactionOutputIdentifiers)
	{
		await sendOutputOrphanedEvent(outputIdentifier);
	}
}

/**
 * Marks a transaction as contested.
 * @internal
 *
 * @param transactionHash - hash of the transaction to mark as contested.
 * @param doubleSpendProofHash - proof there is a competing transaction.
 *
 * @throws an error if called with a new double spend proof when the transaction is finalized, and therefor considered immutable.
 */
export async function setTransactionAsContested(transactionHash: TransactionHash, doubleSpendProofHash: DoubleSpendProofHash): Promise<void>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// Do nothing if we already know that this is contested.
	if(transactionState[transactionHash].contested === doubleSpendProofHash)
	{
		return;
	}

	// Throw an error if this would break the finalization assumptions.
	if(transactionState[transactionHash].finalized)
	{
		throw(new Error(`Failed to set transaction (${transactionHash}) as verified: cannot update transaction with new state after a transaction has finalized`));
	}

	// Update the contested flag to the doublespend proof.
	transactionState[transactionHash].contested = doubleSpendProofHash;

	// Emit a transaction contested event.
	sendTransactionContestedEvent(transactionHash);

	// Get a list of outputs.
	const transactionOutputIdentifiers = await listTransactionOutputs(transactionHash);

	// Iterate over each output and send the output orphaned event.
	for(const outputIdentifier of transactionOutputIdentifiers)
	{
		await sendOutputContestedEvent(outputIdentifier);
	}
}

/**
 * Marks a transaction as verified to be part of the current chain.
 * @internal
 *
 * @param transactionHash - hash of the transaction to mark as verified.
 * @param blockHeaderHash - the block the transaction was included in.
 */
export async function setTransactionAsVerified(transactionHash: TransactionHash, blockHeaderHash: BlockHeaderHash): Promise<void>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// Do nothing if we already know that the transaction is verified in this block.
	if(transactionState[transactionHash].verified === blockHeaderHash)
	{
		return;
	}

	// Throw an error if this would break the finalization assumptions.
	if(transactionState[transactionHash].finalized)
	{
		throw(new Error(`Failed to set transaction (${transactionHash}) as verified: cannot update transaction with new state after a transaction has finalized`));
	}

	// Update the verified flag to the block the transaction was included in.
	transactionState[transactionHash].verified = blockHeaderHash;

	// Emit a transaction verified event.
	await sendTransactionVerifiedEvent(transactionHash);

	// Get a list of outputs.
	const transactionOutputIdentifiers = await listTransactionOutputs(transactionHash);

	// Iterate over each output and send the output orphaned event.
	for(const outputIdentifier of transactionOutputIdentifiers)
	{
		await sendOutputVerifiedEvent(outputIdentifier);
	}
}

/**
 * Marks a transaction as finalized, making it internally immutable.
 * @internal
 *
 * @param transactionHash - hash of the transaction to mark as finalized
 */
export async function setTransactionAsFinalized(transactionHash: TransactionHash): Promise<void>
{
	// Ensure transaction state is fully initialized.
	await initializeTransactionState(transactionHash);

	// Do nothing if the transaction is already finalized.
	if(transactionState[transactionHash].finalized)
	{
		return;
	}

	// Update the finalization flag to true.
	transactionState[transactionHash].finalized = true;

	// Emit transaction finalization event.
	await sendTransactionFinalizedEvent(transactionHash);

	// Get a list of outputs.
	const transactionOutputIdentifiers = await listTransactionOutputs(transactionHash);

	// Iterate over each output and send the output orphaned event.
	for(const outputIdentifier of transactionOutputIdentifiers)
	{
		await sendOutputFinalizedEvent(outputIdentifier);
	}
}

/* Misc. */

/**
 * Validates that a transaction is part of the current chain at the specified block height.
 *
 * @internal
 */
export async function verifyTransactionInclusion(transactionHash: TransactionHash, inclusionHeight: BlockHeight): Promise<void>
{
	// Get transactions block inclusion proof since it is not in the mempool.
	const { pos, merkle } = await getTransactionInclusionProof(transactionHash, inclusionHeight);

	// Get the blocks header in order to verify the transaction inclusion proof.
	const blockHeaderHex = await getBlockHeaderFromHeight(inclusionHeight);

	// Parse the blockheader to extract the merkle root.
	const root = await getMerkleRootFromHeader(blockHeaderHex);

	// Verify that the transaction is included in the block.
	await verifyTransactionInclusionProof(transactionHash, pos, merkle, root);
}
