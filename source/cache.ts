// Import polyfill/shim for indexedDB running on node.
import setGlobalVars from 'indexeddbshim';

// Import IndexedDB improvements with utility functions like get/put/count.
import { openDB } from 'idb';

// Import typing for the promise to the database connection.
import type { IDBPDatabase } from 'idb';

// Import typing information used for the cache abstraction functions.
import type { Address, AddressGetHistoryResponse, BlockHeaderHash, BlockHeaderHex, BlockHeight, TransactionHash, TransactionHex, TransactionGetMerkleResponse } from '@electrum-cash/protocol';

// Import typing for the electrum database.
import type { ElectrumDatabaseOptions, ElectrumDatabase, BlockHeaderWithHexHashAndHeight } from './interfaces/database';

/**
 * @module ElectrumCache
 * @internal
 */

// Used to prevent re-configuring storage settings after the database has been initialized.
let initialized = false;

// Will contain a resolved promise for the IndexedDB connection, once opened.
let databasePromise;

// Set initial default storage location and filename.
let databasePath = './';
let databaseFilename = 'Electrum';

// Configure name of the database, and an arbitrary version number.
const databaseName = 'Electrum';
const schemaVersion = 1;

// Initialize object stores in the database on the schema upgrade call.
function schemaUpgrade(database): void
{
	// Create an object store to hold the block headers and their relation to the block chain.
	const blockHeaderStore = database.createObjectStore('blockHeaders');

	// Create an index for the block headers, allowing for sorting and filtering the data by block heights.
	// NOTE: We explicitly set the block height to not be unique, in order to handle orphaned chains.
	blockHeaderStore.createIndex('indexByHeight', 'blockHeight', { unique: false });

	// Create and object store for finalized address history.
	database.createObjectStore('addressHistory');

	// Create an object stores for transaction related data.
	database.createObjectStore('transactions');
	database.createObjectStore('transactionProofs');

	// Create an object store for arbitrary metadata we use to track the chain state.
	database.createObjectStore('chainState');
}

/**
 * Internal utility function that ensures database filename are string-compatible.
 */
function escapeDatabaseName(name: string): string
{
	return `${name}.sqlite`;
}

/**
 * Internal utility function that ensures the IndexedDB shim is only initialized once.
 */
async function getDatabase(): Promise<IDBPDatabase<ElectrumDatabase>>
{
	// Initialize the database, if needed.
	if(!initialized)
	{
		// Configure the IDB shim to work in NodeJS.
		// Instructions from: https://github.com/indexeddbshim/IndexedDBShim
		const checkOrigin = false;

		// Enable the IndexedDB shim.
		// @ts-ignore
		await setGlobalVars(global, { checkOrigin, escapeDatabaseName, databaseBasePath: databasePath });

		// Initialize the database connection.
		// NOTE: This is not awaited, in order to make the promise available immediately.
		databasePromise = openDB<ElectrumDatabase>(databaseName, schemaVersion, { upgrade: schemaUpgrade });

		// Set the initialized flag to prevent further reconfiguration.
		initialized = true;
	}

	// Return a promise to the database.
	return databasePromise;
}

/**
 * Configures the database aspect of the electrum cache.
 * NOTE: Must be called before the first use of the database.
 */
export async function configureDatabase(databaseOptions: ElectrumDatabaseOptions): Promise<void>
{
	// Throw an error if called after cache has already been initialized.
	if(initialized)
	{
		throw(new Error('Cannot configure storage for cache after it has been initialized.'));
	}

	// Update path and filename used for storing the cached data, if provided.
	databasePath     = (databaseOptions.databasePath     ?? databasePath);
	databaseFilename = (databaseOptions.databaseFilename ?? databaseFilename);
}

/**
 * Store arbitrary chain state numbers in cache.
 */
export async function storeChainStateInCache(key: string, value: number): Promise<void>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Return a promise for requested data.
	await database.put('chainState', value, key);
}

/**
 * Read arbitrary chain state numbers from cache.
 */
export async function getChainStateFromCache(key: string): Promise<number>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	return database.get('chainState', key);
}

/**
 * Stores finalized address history in the cache.
 */
export async function storeAddressHistoryInCache(address: Address, addressHistory: AddressGetHistoryResponse, blockHeight: BlockHeight): Promise<void>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Store the finalized address history and corresponding block height in cache.
	await database.put('addressHistory', { addressHistory, blockHeight }, address);
}

/**
 * Reads finalized address history from the cache.
 */
export async function getAddressHistoryFromCache(address: Address): Promise<{ addressHistory: AddressGetHistoryResponse; blockHeight: BlockHeight }>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Get the address history from the database.
	const result = await database.get('addressHistory', address);

	// Return an empty address history if none was found.
	if(typeof result === 'undefined')
	{
		return { addressHistory: [], blockHeight: 0 };
	}

	// Return a promise for requested data.
	return result;
}

/**
 * Stores a block header in the cache.
 */
export async function storeBlockHeadersInCache(blockHeadersWithHexAndHash: Array<BlockHeaderWithHexHashAndHeight>): Promise<void>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Create a database transaction in order to allow multiple inserts, as well as configuring durability.
	const transaction = database.transaction('blockHeaders', 'readwrite', { durability: 'relaxed' });

	// Initialize an empty list of database operations.
	const operations = [];

	// Add or update each data point, storing a promise for its completion in the transaction list.
	for(const { blockHeaderHash, blockHeaderHex, blockHeaderHeight } of blockHeadersWithHexAndHash)
	{
		operations.push(transaction.store.put({ blockHeaderHex, blockHeaderHeight }, blockHeaderHash));
	}

	// Wait for each transaction to finish, as well as the database provided signal that the transaction has finalized according to durability requirements.
	await Promise.all([ ...operations, transaction.done ]);
}

/**
 * Reads a block header from the cache.
 */
export async function getBlockHeaderFromCache(blockHeaderHash: BlockHeaderHash): Promise<BlockHeaderHex>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Get the block header from the database.
	const { blockHeaderHex } = await database.get('blockHeaders', blockHeaderHash);

	// Return a promise for requested data.
	return blockHeaderHex;
}

/**
 * Reads a link to a block header at a specific chain height from the cache.
 */
export async function getBlockHeaderAtHeightFromCache(blockHeight: BlockHeight): Promise<BlockHeaderHash | undefined>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Get the block header from the database, using the block height index to find the first block matching the height.
	const result = await database.getFromIndex('blockHeaders', 'indexByHeight', IDBKeyRange.bound(blockHeight, blockHeight));

	// If the block header could not be found, return undefined.
	if(typeof result === 'undefined')
	{
		return undefined;
	}

	// Return a promise for requested data.
	return result.blockHeaderHex;
}

/**
 * Reads a linked height for a given block header from the cache.
 */
export async function getHeightForBlockHeaderFromCache(blockHeaderHash: BlockHeaderHash): Promise<BlockHeight>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Get the block header height from the database.
	const { blockHeaderHeight } = await database.get('blockHeaders', blockHeaderHash);

	// Return a promise for requested data.
	return blockHeaderHeight;
}

/**
 * Stores a raw transaction in the cache.
 */
export async function storeTransactionInCache(transactionHash: TransactionHash, transactionHex: TransactionHex): Promise<void>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	await database.put('transactions', transactionHex, transactionHash);
}

/**
 * Reads a raw transaction from the cache.
 */
export async function getTransactionFromCache(transactionHash: TransactionHash): Promise<BlockHeaderHash>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Return a promise for requested data.
	return database.get('transactions', transactionHash);
}

/**
 * Stores a transaction double-spend proof in the cache.
 */
export async function storeTransactionProofInCache(transactionProofIdentity: string, fetchedTransactionProof: TransactionGetMerkleResponse): Promise<void>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	await database.put('transactionProofs', fetchedTransactionProof, transactionProofIdentity);
}

/**
 * Reads a transaction double-spend proof from the cache.
 */
export async function getTransactionProofFromCache(transactionProofIdentity: string): Promise<TransactionGetMerkleResponse>
{
	// Get a link to the database, initializing it if necessary.
	const database = await getDatabase();

	// Return a promise for requested data.
	return database.get('transactionProofs', transactionProofIdentity);
}
