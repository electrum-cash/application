// Import IndexedDB typing used for the electrum database.
import type { DBSchema } from 'idb';

// Import typing information used for the database.
import type { AddressGetHistoryResponse, BlockHeaderHash, BlockHeaderHex, BlockHeight, TransactionHash, TransactionHex, TransactionGetMerkleResponse } from '@electrum-cash/protocol';

import type { ElectrumOptions } from './index';

/**
 * Optional configuration parameters used at electrum database initialization.
 * @internal
 */
export type ElectrumDatabaseOptions = Pick<ElectrumOptions, 'databasePath' | 'databaseFilename'>;

/**
 * Set up an IndexedDB database structure type.
 * @internal
 */
export interface ElectrumDatabase extends DBSchema
{
	'blockHeaders':
	{
		key: BlockHeaderHash;
		value:
		{
			blockHeaderHex: BlockHeaderHex;
			blockHeaderHeight: BlockHeight;
		};
		indexes:
		{
			indexByHeight: 'blockHeaderHeight';
		};
	};

	'addressHistory':
	{
		key: TransactionHash;
		value:
		{
			addressHistory: AddressGetHistoryResponse;
			blockHeight: BlockHeight;
		};
	};

	'transactions':
	{
		key: TransactionHash;
		value: TransactionHex;
	};

	'transactionProofs':
	{
		key: string;
		value: TransactionGetMerkleResponse;
	};

	'chainState':
	{
		key: string;
		value: number;
	};
}

/**
 * @internal
 */
export interface BlockHeaderWithHexHashAndHeight
{
	blockHeaderHex: BlockHeaderHex;
	blockHeaderHash: BlockHeaderHash;
	blockHeaderHeight: BlockHeight;
}
