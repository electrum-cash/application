import { ElectrumProtocolEvents, Address, AddressStatus, BlockHeaderHash, BlockHeight, TransactionHash, OutputIdentifier } from '@electrum-cash/protocol';

// Import state typings.
import type { BlockState, TransactionState, OutputState } from './state';

/**
 * @group Events
 */

/**
 * Emitted when an unrecoverable error has happened in the application layer.
 * @group System
 */
export type SystemFailure = string;

/**
 * Emitted when additional block headers have been verified to be a part of the current longest chain.
 * @group Blockchain
 */
export interface ChainStatus
{
	/** number of blocks that have been reported to be part of the block chain. */
	currentHeight: BlockHeight;

	/** number of blocks that have been verified to be part of the block chain. */
	verifiedHeight: BlockHeight;

	/** percentage of blocks that have been verified to be included in the chain. */
	verifiedPercent: string;
}

/**
 * Emitted when a new block header have been received.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   rejected@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style rejected stroke: #f30
 *   unknown  --> received;
 *   received --> |validated|verified;
 *   competing --> |settled|finalized;
 *   verified ---> |settled|finalized;
 *   verified --> |reorganized|competing;
 *   competing --> |orphaned|rejected;
 *   received ---> |invalidated|rejected;
 * ```
 *
 * @group Blockchain
 */
export interface BlockReceived
{
	/** hash of the received blocks header. */
	blockHash: BlockHeaderHash;

	/** Current state of the block. */
	blockState: BlockState;
}

/**
 * Emitted when additional block headers have been verified to be a part of the current longest chain.
 * @group Blockchain
 */
export interface BlockVerified
{
	/** hash of the verified blocks header. */
	blockHash: BlockHeaderHash;

	/** Current state of the block. */
	blockState: BlockState;
}

/**
 * Emitted when a new transaction has been detected.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 * style received stroke-width:5px
 *   unknown ===> |validated|received;
 *   received ---> |not contested|verified;
 *   verified --> finalized;
 *   verified --> |excluded|discarded;
 *   received --> |doublespent|contested;
 *   contested --> |included|verified;
 *   contested --> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * @group Transaction
 */
export interface TransactionReceived
{
	/** hash of the received transaction. */
	transactionHash: TransactionHash;

	/** Current state of the transaction. */
	transactionState: TransactionState;
}

/**
 * Emitted when a transaction has been verified to be a part of the current chain.
 *
 * *NOTE: if the block including the transaction is **orphaned**, it will revert to its previous `received` or `contested` state.*
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 * style verified stroke-width:5px
 *   unknown ===> |validated|received;
 *   received ==> |not contested|verified;
 *   verified --> finalized;
 *   verified --> |excluded|discarded;
 *   received ==> |doublespent|contested;
 *   contested ==> |included|verified;
 *   contested --> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * @group Transaction
 */
export interface TransactionVerified
{
	/** hash of the transaction that has been verified to be included in the chain. */
	transactionHash: TransactionHash;

	/** Current state of the transaction. */
	transactionState: TransactionState;
}

/**
 * Emitted when a transaction has been orphaned by the current chain, and therefor going from a verified state to either received or contested, depending on the presence of a double-spend proof.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 * style received stroke-width:5px
 * style contested stroke-width:5px
 *   unknown ---> |validated|received;
 *   received === |orphaned|verified;
 *   verified --> finalized;
 *   verified --> |excluded|discarded;
 *   received --> |doublespent|contested;
 *   contested === |orphaned|verified;
 *   contested --> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * @note the mermaid graph above has the arrow directions reversed due to a technical issue.
 *
 * @group Transaction
 */
export interface TransactionOrphaned
{
	/** hash of the transaction that has been orphaned by the current chain. */
	transactionHash: TransactionHash;

	/** Current state of the transaction. */
	transactionState: TransactionState;
}

/**
 * Emitted when a verified transaction has sufficient confirmations in the current chain.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 * style finalized stroke-width:5px
 *   unknown ===> |validated|received;
 *   received ==> |not contested|verified;
 *   verified ==> finalized;
 *   verified --> |excluded|discarded;
 *   received ==> |doublespent|contested;
 *   contested ==> |included|verified;
 *   contested --> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * @note Transaction finality can never be 100%, but for the sake of simplicity this library
 *       only manages chain re-organizations if they are less than {@link BLOCKS_REQUIRED_FOR_FINALIZATION} number of blocks deep.
 *
 * @group Transaction
 */
export interface TransactionFinalized
{
	/** hash of the transaction that has been verified to be included in the chain. */
	transactionHash: TransactionHash;

	/** Current state of the transaction. */
	transactionState: TransactionState;
}

/**
 * Emitted when a double spend proof is detected for a transaction.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 * style contested stroke-width:5px
 *   unknown ===> |validated|received;
 *   received ..-> |not contested|verified;
 *   verified --> finalized;
 *   verified --> |excluded|discarded;
 *   received ==> |doublespent|contested;
 *   contested --> |included|verified;
 *   contested --> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * @group Transaction
 */
export interface TransactionContested
{
	/* hash of the transaction that has been involved in a double-spend situation. */
	transactionHash: TransactionHash;

	/** Current state of the transaction. */
	transactionState: TransactionState;
}

/**
 * Emitted
 * when either a contested transaction is removed the the mempool, or
 * when a verified transaction is removed from the current chain, as a result of a chain re-organization.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   discarded@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style discarded stroke: #f30
 * style discarded stroke-width:5px
 *   unknown ===> |validated|received;
 *   received ===> |not contested|verified;
 *   verified --> finalized;
 *   verified ==> |excluded|discarded;
 *   received ==> |doublespent|contested;
 *   contested ==> |included|verified;
 *   contested ==> |dropped|discarded;
 *   discarded -.-> |rebroadcasted|unknown;
 * ```
 *
 * @group Transaction
 */
export interface TransactionRejected
{
	/** hash of the rejected transaction. */
	transactionHash: TransactionHash;
}

/**
 * Emitted when ...
 * @internal
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   spent@{ shape: dbl-circ }
 *   rejected@{ shape: dbl-circ }
 * style spent stroke: #0f3
 * style rejected stroke: #f30
 *   unknown --> output;
 *   output ----> |dropped or orphaned|rejected;
 *   output --> finalizedOutput;
 *   finalizedOutput <--> input;
 *   output <---> input;
 *   input ---> |dropped or orphaned|output;
 *   input --> finalizedInput;
 *   finalizedInput --> spent;
 * ```
 *
 * @group Output
 */
export interface OutputEventState
{
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputReceived extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputContested extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputRejected extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputVerified extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputOrphaned extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputFinalized extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * TODO: Document me.
 * @group Output
 */
export interface OutputSpent extends OutputEventState
{
	outputIdentifier: OutputIdentifier;
	outputState: OutputState;
}

/**
 * Emitted when a the history of an address has changed.
 * @group Address
 */
export interface AddressUpdate
{
	/** address relevant to the update notification. */
	address: Address;

	/** address status hash. */
	status: AddressStatus;
}

/**
 * List of events emitted by the ElectrumApplication.
 * @event
 * @internal
 */
export interface ElectrumApplicationEvents extends ElectrumProtocolEvents
{
	/**
	 * ...
	 * @eventProperty
	 */
	'SystemFailure': [ SystemFailure ];

	/**
	 * ...
	 * @eventProperty
	 */
	'ChainStatus': [ ChainStatus ];

	/**
	 * ...
	 * @eventProperty
	 */
	'BlockReceived': [ BlockReceived ];

	/**
	 * ...
	 * @eventProperty
	 */
	'BlockVerified': [ BlockVerified ];

	/**
	 * ...
	 * @eventProperty
	 */
	'TransactionReceived': [ TransactionReceived ];

	/**
	 * ...
	 * @eventProperty
	 */
	'TransactionVerified': [ TransactionVerified ];

	/**
	 * ...
	 * @eventProperty
	 */
	'TransactionOrphaned': [ TransactionOrphaned ];

	/**
	 * ...
	 * @eventProperty
	 */
	'TransactionFinalized': [ TransactionFinalized ];

	/**
	 * ...
	 * @eventProperty
	 */
	'TransactionContested': [ TransactionContested ];

	/**
	 * ...
	 * @eventProperty
	 */
	'TransactionRejected': [ TransactionRejected ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputReceived': [ OutputReceived ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputContested': [ OutputContested ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputRejected': [ OutputRejected ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputVerified': [ OutputVerified ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputOrphaned': [ OutputOrphaned ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputFinalized': [ OutputFinalized ];

	/**
	 * ...
	 * @eventProperty
	 */
	'OutputSpent': [ OutputSpent ];

	/**
	 * ...
	 * @eventProperty
	 */
	'AddressUpdate': [ AddressUpdate ];

}
