import type { BlockHeight, BlockHeaderHash, DoubleSpendProofHash, OutputIndex, OutputIdentifier, InputIndex, InputIdentifier } from '@electrum-cash/protocol';

/**
 * The current state of a given transaction, used in transaction life-cycle events.
 *
 * @group State
 */
export interface TransactionState
{
	/** Set to true after a transaction is initially received. */
	received: boolean;

	/** Hash of the block in the current chain this transaction is included in, or false. */
	verified: undefined | BlockHeaderHash;

	/** A double-spend proof showing that the transaction is contested, or false. */
	contested: undefined | DoubleSpendProofHash;

	/** Set to true after sufficient block confirmations, after which it is considered immutable. */
	finalized: boolean;
}

/**
 * The current state of a given transaction output, used in output life-cycle events.
 *
 * @group State
 */
export interface OutputState
{
	/** Set to the spending input identifier when spent, or undefined/false when unspent. */
	spent: undefined | boolean | InputIdentifier;
}

/**
 * Numeric list of stages a block passes through during it's lifetime.
 *
 * ```mermaid
 * graph LR
 *   unknown@{ shape: circ }
 *   finalized@{ shape: dbl-circ }
 *   rejected@{ shape: dbl-circ }
 * style finalized stroke: #0f3
 * style rejected stroke: #f30
 *   unknown  --> received;
 *   received --> |validated|verified;
 *   competing --> |settled|finalized;
 *   verified ---> |settled|finalized;
 *   verified --> |reorganized|competing;
 *   competing --> |orphaned|rejected;
 *   received ---> |invalidated|rejected;
 * ```
 *
 * @group State
 */
export enum BlockStages
{
	/** The block is not yet known by the network. */
	UNKNOWN = 0,

	/** The block has been seen but not yet validated locally. */
	RECEIVED = 1,

	/** The block has been seen and validated locally. */
	VERIFIED = 3,

	/** Another block competing for the same blockheight has been observed. */
	CONTESTED = 2,

	/** The block is finalized and considered immutable. */
	FINALIZED = 4,
}

/**
 * The current state of a given block, used in block life-cycle events.
 *
 * @group State
 */
export interface BlockState
{
	/** The chain height this block was included in, or -1 if not part of the current chain. */
	height: BlockHeight;

	/** Number of blocks in the current chain that have extended this block. */
	confirmations: number;

	/** Boolean flag indicating if another block have been seen at the same height. */
	contested: boolean;

	/** Set to true after sufficient confirmations, after which it is considered immutable. */
	finalized: boolean;

	/** The current stage in the block life-cycle. */
	stage: BlockStages;
}
