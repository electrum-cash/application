// Import required type definitions.
import type { ElectrumServers } from '@electrum-cash/servers';

// Export state and events to include them in documentation.
export * from './events';
export * from './state';

/**
 * Optional configuration parameters used at electrum application initialization.
 * @group Setup
 */
export interface ElectrumOptions
{
	/** Optional list of electrum servers to connect to. */
	electrumServers?: ElectrumServers;

	/** Optional flag to enable/disable encryption for electrum network connections. */
	electrumEncryption?: boolean;

	/** Optional database path, only used on non-browser runtimes. */
	databasePath?: string;

	/** Optional database filename, only used on non-browser runtimes.  */
	databaseFilename?: string;
}

/**
 * @internal
 */
export type ElectrumNetworkOptions = Pick<ElectrumOptions, 'electrumServers' | 'electrumEncryption'>;
